from matplotlib import pyplot as plot


up_key=[1,2,3,4,5,6]
val=[1,20,30,40,50]


for up in up_key:
    y_position=0
    for teams in  sorted(val):
        plot.bar(up,teams,bottom=y_position)
        y_position=y_position+teams



plot.xlabel("values")
plot.ylabel("mht")
plot.title("Stacked bar chart")
plot.legend('1',framealpha=.5)
plot.show()