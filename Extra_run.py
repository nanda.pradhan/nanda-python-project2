import master
from matplotlib import pyplot as plot

#read data from csv file
matches_file_rows=master.file_data_read('CSV-FILE/matches.csv')
deliveris_file_rows=master.file_data_read('CSV-FILE/deliveries.csv')

#reading match ids from match file
match_ids=master.read_match_ids('2016',matches_file_rows)

#reading data from deliveris file based on id
deliveris_data_2016=master.fetch_data_by_id(match_ids,deliveris_file_rows)

#calculating extra run per id
extra_run_per_team={}
for data in deliveris_data_2016:
    split_data = data.split(',')
    if split_data[3] in extra_run_per_team.keys():
        extra_run_per_team[split_data[3]] += int(split_data[16])
    else:
        extra_run_per_team.update({split_data[3]: int(split_data[16])})


master.diaplay_bar_chart(extra_run_per_team,'Extra run','Team name','Extra run Conceed per team in 2016')






