import master

#reading data from csv file
file_data=master.file_data_read('CSV-FILE/matches.csv')

#fatching years from data
# year_list=master.fatch_year(file_data)
year_list={}
for sdata in range(1, (len(file_data) - 2)):
    split_rows = file_data[sdata].split(',')
    year_list.update({split_rows[1]:0})


for data in range(1, (len(file_data) - 2)):
    split_data=file_data[data].split(',')
    year_list[split_data[1]]+=1

master.display_bar(year_list,'Year','Matches','Matches played per year')

