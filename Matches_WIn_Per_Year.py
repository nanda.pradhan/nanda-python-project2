import master
from matplotlib import pyplot as plot

#reading data from csv file
file_data=master.file_data_read('CSV-FILE/matches.csv')

#Finding team names from data
team_names_list=[]
for data in range(1,(len(file_data)-1)):
    match_name=file_data[data].split(',')
    team1=match_name[4]
    team2=match_name[5]
    team_names_list.append(team1)

#find years from data
year_name_list=master.fatch_year(file_data)

#converting team and year list to set
team_list=set(team_names_list)
year_list=set(year_name_list)

color=['cornflowerblue','orange','blue','crimson','blueviolet','brown','cyan','darkgray','green','deepskyblue','indigo','lawngreen','lightsalmon','red','gray']
color_dict={}
i=0
for data in sorted(team_list):
    color_dict.update({data:color[i]})
    i+=1

winner_count_per_year={}
for i in range(1,(len(year_name_list)-1)):
    year=year_name_list[i]
    winner_count={}
    for j in range(1,(len(file_data)-1)):
        split_data=file_data[j].split(',')
        team=split_data[4]
        if split_data[1]==year:
            if team in winner_count.keys():
                winner_count[team] += 1
            else:
                winner_count.update({team: 0})
    winner_count_per_year.update({year:winner_count})




plot.rcParams['figure.figsize']=(20,8)
margins_value={"left":.04,"bottom":0.065,"right":.8,"top":.95}
plot.subplots_adjust(**margins_value)


#ploting data in stackeed bar chart
for year in sorted(winner_count_per_year.keys()):
    y_position=0
    for data in sorted(winner_count_per_year[year].keys()):
        plot.bar(year,winner_count_per_year[year][data],bottom=y_position,color=color_dict[data])
        y_position=y_position+winner_count_per_year[year][data]




plot.xlabel("Seasons")
plot.ylabel("Matches Win")
plot.title("Stacked bar chart of No of matches winn per year")
plot.legend(sorted(team_list),loc=2,bbox_to_anchor=(1.05,1))
ax = plot.gca()
leg = ax.get_legend()
i=0
for data in sorted(color_dict.keys()):
    leg.legendHandles[i].set_color(color_dict[data])
    i+=1

#
# leg.legendHandles[1].set_color('yellow')

plot.show()

