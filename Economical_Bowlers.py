from matplotlib import pyplot as plot
import master

#readinf file data from file
matches_file_rows=master.file_data_read('CSV-FILE/matches.csv')
deliveris_file_rows=master.file_data_read('CSV-FILE/deliveries.csv')

#reading match ids of year 2015
match_ids=master.read_match_ids('2015',matches_file_rows)

#fetach data by id from deliveries file
deliveris_data_2015=master.fetch_data_by_id(match_ids,deliveris_file_rows)

#calculating economy baller
ball_count={}
run_count={}
economy_baller={}
for data in deliveris_data_2015:
    split_data = data.split(',')
    if split_data[8] in run_count.keys() and ball_count.keys():
        run_count[split_data[8]] += int(split_data[17])
        ball_count[split_data[8]] += 1
        economy_baller[split_data[8]]=run_count[split_data[8]]/float(ball_count[split_data[8]])
    else:
        run_count.update({split_data[8]:int(split_data[17])})
        ball_count.update({split_data[8]:1})
        economy_baller.update({split_data[8]:0})


master.diaplay_bar_chart(economy_baller,'Baller name','Economical value','Economical baller of 2015')






