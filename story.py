import master

#reading data from csv file
file_data=master.file_data_read('CSV-FILE/matches.csv')

#reading man of the match
man_of_the_match=[]
for data in range(1,(len(file_data)-1)):
    split_data=file_data[data].split(',')
    man_of_the_match.append(split_data[13])
man_of_the_match_count={}

for player in man_of_the_match:
    count=0
    for player1 in man_of_the_match:
        if player==player1:
            count+=1
    man_of_the_match_count.update({player:count})

master.display_bar_top_10(man_of_the_match_count,'player name','Man of the match win','Player win maximun man of the match')

