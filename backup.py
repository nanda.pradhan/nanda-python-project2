import master
from matplotlib import pyplot as plot

#reading data from csv file
file_data=master.file_data_read('CSV-FILE/matches.csv')

#Finding team names from data
team_names_list=[]
for data in range(1,(len(file_data)-1)):
    match_name=file_data[data].split(',')
    team1=match_name[4]
    team_names_list.append(team1)

#find years from data
year_name_list=master.fatch_year(file_data)

#converting team and year list to set
team_list=set(team_names_list)
year_list=set(year_name_list)

#counting matches win by team in each year
winner_count_per_year={}
for year in sorted(year_list):
    winner_count = {}
    for team in team_list:
        team_name=team
        win_count=0
        for data in range(1, (len(file_data) - 1)):
            split_data = file_data[data].split(',')
            if team_name==split_data[10] and split_data[1]==year:
                win_count=win_count+1

        winner_count.update({team_name:win_count})
    winner_count_per_year.update({year:winner_count})

#ploting data in stackeed bar chart
for year in sorted(winner_count_per_year.keys()):
    y_position=0
    for data in sorted(winner_count_per_year[year].keys()):
        plot.bar(year,winner_count_per_year[year][data],bottom=y_position)
        y_position=y_position+winner_count_per_year[year][data]

plot.xlabel("Seasons")
plot.ylabel("Matches_Win")
plot.title("Stacked bar chart of No of matches winn per year")
plot.legend(sorted(team_list),framealpha=.5)
plot.show()

