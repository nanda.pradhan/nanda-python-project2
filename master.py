from matplotlib import pyplot as plot


key_list=[]
value_list=[]
def diaplay_bar_chart(data_to_display,x_label,y_label,title):
    for key in sorted(data_to_display.keys(), key=data_to_display.get):
        key_list.append(key)
        value_list.append(data_to_display[key])

    plot.rcParams['figure.figsize'] = (20, 8)
    margins_value = {"left": .05, "bottom": 0.295, "right":.99, "top": .95}
    plot.subplots_adjust(**margins_value)

    plot.bar(key_list[:10], value_list[:10], color="red")
    plot.xticks(key_list[:10], rotation='vertical')
    plot.xlabel(x_label)
    plot.ylabel(y_label)
    plot.title(title)
    plot.show()


#read file from csv
def file_data_read(path):
    file_reader = open(path, 'r')
    file_data = file_reader.read()
    split_data=file_data.split('\n')
    return split_data



#plot bar chart
display_details=[]
def display_bar(match_count_per_year,x_label,y_label,title):
    key = []
    value = []
    for k in sorted(match_count_per_year.keys()):
        key.append(k)
        value.append(match_count_per_year[k])

    plot.bar(key, value, color="red", tick_label=key)
    plot.xlabel(x_label)
    plot.ylabel(y_label)
    plot.title(title)
    plot.show()

def display_bar_vertical(match_count_per_year,x_label,y_label,title):
    key = []
    value = []
    for k in sorted(match_count_per_year.keys()):
        key.append(k)
        value.append(match_count_per_year[k])

    plot.bar(key, value, color="red", tick_label=key)
    plot.xlabel(x_label)
    plot.ylabel(y_label)
    plot.title(title)
    plot.xticks(key, rotation='vertical')
    plot.show()



#fatching years from data
year_list=[]
def fatch_year(split_data):
    for sdata in range(0, (len(split_data) - 2)):
        split_rows = split_data[sdata].split(',')
        year_list.append(split_rows[1])
    return year_list


#read match id of specific year

match_ids=[]
def read_match_ids(year,matches_file_rows):
    for i in range(1, (len(matches_file_rows) - 1)):
        split_year = matches_file_rows[i].split(',')
        if split_year[1] ==year:
            match_ids.append(split_year[0])
    return match_ids


#fetch data by year
data_by_id=[]
def fetch_data_by_id(match_id,file):
    for data in file:
        sp = data.split(',')
        if sp[0] in match_ids:
            data_by_id.append(data)
    return data_by_id

#display bar chart of top 10
key_list=[]
value_list=[]
def display_bar_top_10(data_to_print,x_label,y_label,title):
    for key in sorted(data_to_print.keys(), key=data_to_print.get):
        key_list.append(key)
        value_list.append(data_to_print[key])
    plot.bar(key_list[len(key_list)-11:len(key_list)-1], value_list[len(value_list)-11:len(value_list)-1],)
    plot.xticks(key_list[len(key_list)-11:len(key_list)-1], rotation='vertical')
    plot.xlabel(x_label)
    plot.ylabel(y_label)
    plot.title(title)

    plot.show()


